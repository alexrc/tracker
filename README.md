# Tracker

Indoor localization app. Codename Tracker.

This app is supposed to perform indoor localization eventually.

## Current Controls

Map-like controls for the UI: pinch to zoom, drag to pan.

Hold your finger on the screen to take a sample of surrounding wifis. The markers look cool, btw, don't they? :)

Take a look at the Android debug log for more info on what's happening. For each point, every info android gives about surrounding wifis is being stored.

# TODO 

* Define project license
* Display somehow obtained info 
* Serialize obtained info
* Define settings
* Check screen orientation/lifecycle issues
* Implement KNN
* Estimate location with KNN
* Upload custom maps
