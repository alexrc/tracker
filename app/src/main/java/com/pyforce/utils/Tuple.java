package com.pyforce.utils;

/**
 * Created by Alex Coto on 13/2/16.
 *
 * TODO: substitute usages of this class for <code>Map</code>.
 */
public class Tuple<T,R> {

    public Tuple(T A, R B){
        this.A = A;
        this.B = B;
    }

    public T A;
    public R B;

    @Override
    public String toString() {
        return B.toString();
    }
}
