package com.pyforce.utils;

/**
 * Created by Alex Coto on 1/3/16.
 */
public abstract class ConnectionCallback {
    public void onSuccess(){}
    public void onFailure(){}
}
