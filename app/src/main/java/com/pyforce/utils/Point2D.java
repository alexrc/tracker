package com.pyforce.utils;

import android.graphics.PointF;

/**
 * A 2-dimensional point.
 */
public class Point2D {
    public double X;
    public double Y;

    static double eps = 1e-7;

    public Point2D(PointF point){
        X = point.x;
        Y = point.y;
    }

    public Point2D(double currentX, double currentY) {
        X = currentX;
        Y = currentY;
    }

    public Point2D copy() {
        return new Point2D(X,Y);
    }

    public Point2D scale(float scale){
        return new Point2D(X*scale, Y*scale);
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Point2D)) return false;
        Point2D other = (Point2D) o;

        return Math.abs(other.X - X) < eps && Math.abs(other.Y - Y) < eps;
    }
}
