package com.pyforce.utils;

import android.support.annotation.NonNull;

/**
 * Helper class for sorting fingerprints according to distance.
 */
public class RSSISortableVector implements Comparable<RSSISortableVector>{
    public Point2D position;
    public double distance;

    public RSSISortableVector(Point2D position, double distance){
        this.position = position;
        this.distance = distance;
    }

    @Override
    public int compareTo(@NonNull RSSISortableVector another) {
        return Double.compare(distance,another.distance);
    }
}
