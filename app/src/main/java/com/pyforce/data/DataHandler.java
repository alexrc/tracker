package com.pyforce.data;

import android.content.Context;
import android.net.Uri;
import android.net.wifi.ScanResult;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.FileAsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestHandle;
import com.loopj.android.http.RequestParams;
import com.pyforce.data.callbacks.DownloadedFileCallback;
import com.pyforce.data.dto.Fingerprint;
import com.pyforce.data.dto.Floor;
import com.pyforce.data.dto.LocationData;
import com.pyforce.data.dto.WifiData;
import com.pyforce.utils.ConnectionCallback;
import com.pyforce.utils.Point2D;
import com.pyforce.utils.Tuple;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.entity.ByteArrayEntity;
import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.message.BasicHeader;

/**
 * Handles data (kind of a model).
 *
 * Can notify a view through the FloorChangeListener interface.
 *
 * Currently holds multiple floor information.
 *
 * Will handle dynamic data loading/saving
 * from bundles or through the network.
 */
public class DataHandler {

    private static final String DEBUG_TAG = "DataHandler";

    // Path constants
    private static final String API_PATH = "api";
    private static final String CONTENT_PATH = "Content/Images";
    private final Context mContext;

    // Current Ids
    private int mCurrentPlaceId = -1;
    private int mCurrentFloorId = -1;
    private int mDeviceId = -1;
    private String mDeviceHash;

    // The address of the currently connected server
    private String mServerAddress;

    private ListAdapter floorAdapter;
    private ListAdapter placeAdapter;
    private final ArrayList<Tuple<Integer,String>> floorNames;
    private final ArrayList<Tuple<Integer,String>> placeNames;

    private volatile Floor mFloorCache = null;

    AsyncHttpClient mHttpClient = new AsyncHttpClient();

    private static volatile DataHandler instance;
    private RequestHandle connectionRequest;

    public static DataHandler get(){
        return instance;
    }

    Stack<Fingerprint> undoStack = new Stack<>();

    public DataHandler(Context ctx, String deviceHash){

        mContext = ctx;
        mDeviceHash = deviceHash;

        floorNames = new ArrayList<>();
        floorAdapter = new ArrayAdapter<>(
                ctx,
                android.R.layout.simple_list_item_1,
                floorNames);

        placeNames = new ArrayList<>();
        placeAdapter = new ArrayAdapter<>(
                ctx,
                android.R.layout.simple_list_item_1,
                placeNames);

        instance = this;
    }

    public void connect(String serverAddress, final ConnectionCallback callback){
        mServerAddress = serverAddress;

        try {

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("DeviceHash", mDeviceHash);

            connectionRequest = mHttpClient.post(
                    mContext,
                    urlFor("Devices"),
                    new StringEntity(jsonObject.toString()),
                    "application/json",
                    new JsonHttpResponseHandler() {

                        @Override
                        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                            try {
                                mDeviceId = response.getInt("Id");
                            } catch (JSONException e) {
                                Log.e(DEBUG_TAG, e.getMessage());
                            }

                            callback.onSuccess();
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers,
                                              Throwable throwable, JSONObject errorResponse) {
                            Toast.makeText(mContext, "Failure connecting to server.", Toast.LENGTH_SHORT).show();
                            callback.onFailure();
                        }
                    }
            );
        } catch (UnsupportedEncodingException | JSONException e) {
            Log.e(DEBUG_TAG, e.getMessage());
        }
    }

    public void loadPlaceListAsync(final JsonHttpResponseHandler callback) {
        mHttpClient.get(
                urlFor("Places"),
                new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                        placeNames.clear();
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                placeNames.add(new Tuple<>(
                                        response.getJSONObject(i).getInt("Id"),
                                        response.getJSONObject(i).getString("Name")
                                ));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        if (callback!= null)
                            callback.onSuccess(statusCode, headers, response);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        callback.onFailure(statusCode, headers, responseString, throwable);
                    }
                }
        );
    }

    public void loadFloorListForCurrentPlaceAsync(final JsonHttpResponseHandler callback){
        RequestParams params = new RequestParams();
        params.add("Place", String.valueOf(mCurrentPlaceId));

        this.floorNames.clear();

        String url = urlFor("Floors");

        mHttpClient.get(
            url,
            params,
            new JsonHttpResponseHandler(){
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                    try {
                        for (int i = 0; i < response.length(); i++) {
                                floorNames.add(new Tuple<>(
                                        response.getJSONObject(i).getInt("Id"),
                                        response.getJSONObject(i).getString("Name")
                                ));
                        }

                        if (callback != null){
                            callback.onSuccess(statusCode, headers, response);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    callback.onFailure(statusCode, headers, responseString, throwable);
                }
            });
    }

    public void loadCurrentFloorData(final JsonHttpResponseHandler callback){
        mHttpClient.get(
                urlFor("Floors", mCurrentFloorId),
                new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Log.d(DEBUG_TAG, "loadCurrentFloorData success!");

                try {
                    mFloorCache = new Floor(response);
                    if (callback != null)
                        callback.onSuccess(statusCode, headers, response);
                    listener.onFloorChange(mFloorCache);
                } catch (JSONException e) {
                    Log.e(DEBUG_TAG, "Error while parsing floor JSON");
                }
            }

        });
    }

    public void loadCurrentFloorImageUri(final DownloadedFileCallback callback){
        // TODO: Is creating a temporary file okay?
        File tmp = null;
        try {
            tmp = File.createTempFile("temp", null);
        } catch (IOException e) {
            e.printStackTrace();
        }

        mHttpClient.get(urlForImage(mCurrentFloorId), new FileAsyncHttpResponseHandler(tmp) {
            @Override
            public void onFailure(int i, Header[] headers, Throwable throwable, File file) {
                Log.d(DEBUG_TAG, "Failure receiving file");
            }

            @Override
            public void onSuccess(int i, Header[] headers, File file) {
                Log.d(DEBUG_TAG, "Successfully received file!");
                callback.onFileDownloaded(file);
            }
        });
    }

    public void clearCurrentFloorData() {
        RequestParams params = new RequestParams();
        params.add("Floor", String.valueOf(mCurrentFloorId));

        mHttpClient.delete(null,
                urlFor("Fingerprints"),
                new Header[]{
                        new BasicHeader("Floor", String.valueOf(mCurrentFloorId))
                },
                params,
                new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        Log.d(DEBUG_TAG, "Success on deletion!");
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        Log.e(DEBUG_TAG, "Error deleting fingerprints");
                    }
                });
    }

    public void placeFingerprint(Point2D position, List<List<ScanResult>> info) {

        RequestParams rp = new RequestParams(){{
            // TODO: Authentication here
        }};

        final Fingerprint fingerprint = new Fingerprint(position, info, mCurrentFloorId);

        HttpEntity entity = null;
        try {
            entity = new ByteArrayEntity(fingerprint.toJson().getBytes("UTF-8"));
        } catch (UnsupportedEncodingException | JSONException e) {
            e.printStackTrace();
        }

            mHttpClient.post(null,
                    urlFor("Fingerprints"),
                    entity,
                    "application/json",
                    new JsonHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                            Log.d(DEBUG_TAG, "Successfully created fingerprint on server");
                            try {
                                fingerprint.Id = response.getInt("Id");
                            } catch (JSONException e) {
                                Log.e(DEBUG_TAG, e.getMessage());
                            }
                            undoStack.push(fingerprint);
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                            Log.d(DEBUG_TAG, "Failed to create fingerprint on server");
                        }
                    }
            );
    }

    public void setCurrentFloor(int floorIndex){
        mCurrentFloorId = floorIndex;

        loadCurrentFloorData(null);
    }

    public void setCurrentPlace(int placeIndex){
        mCurrentPlaceId = placeIndex;

        loadFloorListForCurrentPlaceAsync(null);
    }

    public String urlFor(String resource){
        String built = String.format("http://%s/%s/%s/", mServerAddress, API_PATH, resource);
        Log.i(DEBUG_TAG, built);
        return built;
    }

    public String urlFor(String resource, int id){
        return urlFor(resource) + id;
    }

    public String urlForImage(int floorId){
        String built = String.format("http://%s/%s/%s.jpg", mServerAddress, CONTENT_PATH, floorId);
        Log.i(DEBUG_TAG, built);
        return built;
    }

    public ListAdapter getFloorAdapter() {
        return floorAdapter;
    }

    public ListAdapter getPlaceAdapter() {
        return placeAdapter;
    }

    public int getDeviceId() {
        return mDeviceId;
    }

    public void cancelPendingConnections() {
        if (connectionRequest != null && !connectionRequest.isFinished()){
            connectionRequest.cancel(true);
        }
    }

    public Fingerprint undoLastFingerprint() {
        if (undoStack.size() > 0){
            Fingerprint fpToDelete = undoStack.pop();

            mHttpClient.delete(null,
                    urlFor("Fingerprints") + '/' + fpToDelete.Id ,
                    new Header[]{},
                    new RequestParams(),
                    new JsonHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                            Log.d(DEBUG_TAG, "Success on deletion!");
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                            Log.e(DEBUG_TAG, "Error deleting fingerprints");
                        }
                    });

            return fpToDelete;
        } else {
            return null;
        }
    }

    public Uri getUrlForDeviceMap() {
        return Uri.parse("http://" + mServerAddress + "/devices");
    }

    public void sendLocationData(List<ScanResult> results, JsonHttpResponseHandler responseHandler){
        try {
            LocationData ld = new LocationData(results, DataHandler.get().getDeviceId());

            mHttpClient.post(
                    null,
                    DataHandler.get().urlFor("LocationData"),
                    new StringEntity(ld.toJson().toString()),
                    "application/json",
                    responseHandler
                    );
        } catch (UnsupportedEncodingException | JSONException e) {
            Log.e(DEBUG_TAG, e.getMessage());
        }
    }

    // TODO: Mix these two methods.
    public void sendWifiData(List<WifiData> results, JsonHttpResponseHandler responseHandler){
        try {
            LocationData ld = new LocationData(results, DataHandler.get().getDeviceId());

            mHttpClient.post(
                    null,
                    DataHandler.get().urlFor("LocationData"),
                    new StringEntity(ld.toJson().toString()),
                    "application/json",
                    responseHandler
            );
        } catch (UnsupportedEncodingException | JSONException e) {
            Log.e(DEBUG_TAG, e.getMessage());
        }
    }

    public interface FloorChangeListener{
        void onFloorChange(Floor newFloor);
    }
    public void setFloorChangeListener(FloorChangeListener l){this.listener = l;}
    FloorChangeListener listener;

}
