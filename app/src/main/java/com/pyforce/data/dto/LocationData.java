package com.pyforce.data.dto;

import android.net.wifi.ScanResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by Alex Coto on 16/2/16.
 *
 * TODO: Send timestamp. If a queue is implemented when offline,
 * on retrospective, all data will be eventually available.
 */
public class LocationData {
    public String DetectedWifis;
    public int DeviceId;
//    public Date Timestamp;

    public LocationData(List<ScanResult> scan, final int deviceId) throws JSONException {

        JSONArray j = new JSONArray();

        for (final ScanResult result: scan){
            j.put(new JSONObject(){{
                put("Level", result.level);
                put("BSSID", result.BSSID);
                put("SSID", result.SSID);
            }});
        }

        DetectedWifis = j.toString();

        DeviceId = deviceId; // TODO: Fix
//        Timestamp = new Date();
    }

    public LocationData(Iterable<WifiData> scan, final int deviceId) throws JSONException {

        JSONArray j = new JSONArray();

        for (final WifiData result: scan){
            j.put(new JSONObject(){{
                put("Level", result.RSSIAverage);
                put("BSSID", result.BSSID);
                put("SSID", result.SSID);
            }});
        }

        DetectedWifis = j.toString();

        DeviceId = deviceId; // TODO: Fix
//        Timestamp = new Date();
    }

    public JSONObject toJson() throws JSONException {
        JSONObject j = new JSONObject();

        j.put("DetectedWifis", DetectedWifis);
        j.put("DeviceId", DeviceId);


        // TODO: Fix date format
//        String formatted = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.US)
//                .format(Timestamp);
//        String formattedDate = formatted.substring(0, 22) + ":" + formatted.substring(22);
//
//        j.put("Timestamp", formattedDate);

        return j;
    }
}
