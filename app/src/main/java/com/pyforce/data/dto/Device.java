package com.pyforce.data.dto;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Alex Coto on 16/2/16.
 */
public class Device {
    public int Id;
    public String DeviceHash;

    public Device(JSONObject j) throws JSONException {
        Id = j.getInt("Id");
        DeviceHash = j.getString("DeviceHash");
    }
}
