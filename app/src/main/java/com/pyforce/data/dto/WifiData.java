package com.pyforce.data.dto;

import android.net.wifi.ScanResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by Alex Coto on 11/2/16.
 */
public class WifiData {
    public int Id;

    public String BSSID;
    public String SSID;

    public double RSSIAverage;
    public double SampleCount;
    public double Variance;

    public int FingerprintId;

    public WifiData(String BSSID, String SSID, double levelAverage, int sampleCount, double variance){
        Id = -1;
        FingerprintId = -1;

        this.BSSID = BSSID;
        this.SSID = SSID;

        RSSIAverage = levelAverage;
        SampleCount = sampleCount;
        Variance = variance;
    }

    public WifiData(JSONObject jo) throws JSONException {
        Id = jo.getInt("Id");
        BSSID = jo.getString("BSSID");
        SSID = jo.getString("SSID");

        RSSIAverage = jo.getDouble("RSSIAverage");
        SampleCount = jo.getInt("SampleCount");
        Variance = jo.getDouble("Variance");

        // TODO: Check if needed
        FingerprintId = jo.getInt("FingerprintId");
    }

    public JSONObject toJson() throws JSONException {
        JSONObject json = new JSONObject();

        json.put("BSSID",BSSID);
        json.put("SSID",SSID);
        json.put("RSSIAverage",RSSIAverage);
        json.put("SampleCount",SampleCount);
        json.put("Variance",Variance);
        json.put("FingerprintId",FingerprintId);

        return json;
    }
}
