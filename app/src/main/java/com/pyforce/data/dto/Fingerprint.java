package com.pyforce.data.dto;

import android.net.wifi.ScanResult;

import com.pyforce.utils.Point2D;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Alex Coto on 11/2/16.
 */
public class Fingerprint {
    public int Id;
    public boolean Active;

    public double X;
    public double Y;

    public int FloorId;
    public List<WifiData> Wifis = new ArrayList<>();

    public Fingerprint(Point2D position, List<List<ScanResult>> info, int currentFloor){
        X = position.X;
        Y = position.Y;

        Active = true;
        FloorId = currentFloor;

        HashMap<String, List<ScanResult>> bssid2ScanInfo = new HashMap<>();

        for (List<ScanResult> scanResults :
                info) {

            for (ScanResult wifi: scanResults){
                if (bssid2ScanInfo.containsKey(wifi.BSSID)){
                    bssid2ScanInfo.get(wifi.BSSID).add(wifi);
                } else
                {
                    bssid2ScanInfo.put(wifi.BSSID, new ArrayList<ScanResult>());
                    bssid2ScanInfo.get(wifi.BSSID).add(wifi);
                }
            }
        }

        for (String mac : bssid2ScanInfo.keySet()) {
            ScanResult wifiInfo = bssid2ScanInfo.get(mac).get(0);
            int levelSum  = 0;
            int count = 0;
            for (ScanResult scan : bssid2ScanInfo.get(mac)){
                levelSum += scan.level;
                count += 1;
            }
            double avg = levelSum / count;

            double variance = 0;
            for (ScanResult scan: bssid2ScanInfo.get(mac)){
                variance += Math.pow(avg - scan.level, 2);
            }
            variance /= count;

            Wifis.add(new WifiData(wifiInfo.BSSID,wifiInfo.SSID, avg, count, variance));
        }
    }

    public Fingerprint(JSONObject jo) throws JSONException {
        Id = jo.getInt("Id");
        Active = jo.getBoolean("Active");
        X = jo.getInt("X");
        Y = jo.getInt("Y");
        FloorId = jo.getInt("FloorId");

        JSONArray wifis = jo.getJSONArray("Wifis");
        for (int i = 0; i < wifis.length(); i++) {
            Wifis.add(new WifiData(
                wifis.getJSONObject(i))
            );
        }
    }


    public String toJson() throws JSONException {
        JSONObject jobj = new JSONObject();

        jobj.put("Active", Active);
        jobj.put("X", X);
        jobj.put("Y", Y);
        jobj.put("FloorId", FloorId);

        JSONArray wifis = new JSONArray();
        for (WifiData wifi : Wifis) {
            wifis.put(wifi.toJson());
        }

        jobj.put("Wifis", wifis);
        return jobj.toString();
    }

    public Point2D getPosition(){ return new Point2D(X,Y);}

    public Point2D getScaledPosition(float scale){ return new Point2D(X*scale,Y*scale);}

    public List<String> getAllMacs() {
        List<String> macs = new ArrayList<>();

        for (WifiData wifi : Wifis) {
            macs.add(wifi.BSSID);
        }

        return macs;
    }

}
