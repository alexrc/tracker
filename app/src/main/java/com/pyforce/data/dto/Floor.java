package com.pyforce.data.dto;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Model for a floor.
 *
 * - Id
 * - Name
 * - Image Uri
 * - List of fingerprints
 *
 */
public class Floor {
    private static final String DEBUG_TAG = "Floor";

    public int Id;
    public String Name;
    public double OffsetX;
    public double OffsetY;
    public int PlaceId;
    public double PixelsPerMeter;
    public List<Fingerprint> Fingerprints;


    public Floor(JSONObject j) throws JSONException {
        Id = j.getInt("Id");
        Name = j.getString("Name");
        OffsetX = j.optDouble("OffsetX");
        OffsetY = j.optDouble("OffsetY");
        PlaceId = j.optInt("PlaceId");
        PixelsPerMeter = j.optDouble("PixelsPerMeter");

        Fingerprints = new ArrayList<>();

        JSONArray fp = j.getJSONArray("Fingerprints");
        for (int i = 0; i < fp.length(); i++) {
            JSONObject jsonFingerprint = fp.getJSONObject(i);
            Fingerprints.add(new Fingerprint(jsonFingerprint));
        }
    }

    @Override
    public String toString() {
        return Name;
    }

    public Iterable<Fingerprint> getAllFingerprints() {
        return Fingerprints;
    }

}
