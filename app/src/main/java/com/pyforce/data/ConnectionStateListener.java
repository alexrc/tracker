package com.pyforce.data;

/**
 * Created by Alex Coto on 1/3/16.
 */
public abstract class ConnectionStateListener {
    public abstract void onConnected();
    public abstract void onDisconnected();
}
