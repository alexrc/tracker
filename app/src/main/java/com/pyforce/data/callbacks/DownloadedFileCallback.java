package com.pyforce.data.callbacks;

import java.io.File;

/**
 * Created by Alex Coto on 12/2/16.
 */
public interface DownloadedFileCallback {
    void onFileDownloaded(File fileUri);
}
