package com.pyforce.data;

import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by Alex Coto on 4/5/16.
 */
public class CircularQueue<T> implements Iterable<T> {

    ConcurrentLinkedQueue<T> data;
    int count = 0;
    int capacity;

    public CircularQueue(int capacity) {
        this.capacity = capacity;
        data = new ConcurrentLinkedQueue<>();
    }

    public void enqueue(T elem){

        if (count >= capacity){
            data.poll(); // Simply discard the first element
        }

        data.add(elem);
        count++;
    }

    public T dequeue(){
        count--;
        return data.poll();
    }

    @Override
    public Iterator<T> iterator() {
        return data.iterator();
    }
}
