package com.pyforce.data;

import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;

import com.pyforce.data.dto.WifiData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Alex Coto on 4/5/16.
 */
public class IntensityFilters {
    public static List<WifiData> mean(Iterable<List<ScanResult>> results){

        HashMap<String, WifiData> mac2FilteredData = new HashMap<>();

        for (List<ScanResult> resultList : results){
            for (ScanResult result:
                 resultList) {

                if (mac2FilteredData.containsKey(result.BSSID)){
                    WifiData current = mac2FilteredData.get(result.BSSID);
                    current.RSSIAverage += result.level;
                    current.SampleCount++;
                } else{
                    // TODO: Fix variance
                    mac2FilteredData.put(result.BSSID,
                            new WifiData(result.BSSID, result.SSID, result.level, 1, -1));
                }

            }
        }

        ArrayList<WifiData> filteredWifis = new ArrayList<>();

        for(WifiData data : mac2FilteredData.values()){
            data.RSSIAverage /= data.SampleCount;
            filteredWifis.add(data);
        }

        return filteredWifis;
    }
}
