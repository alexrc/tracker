package com.pyforce.tracker;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.Nullable;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.ResponseHandlerInterface;
import com.pyforce.data.CircularQueue;
import com.pyforce.data.DataHandler;
import com.pyforce.data.IntensityFilters;
import com.pyforce.data.dto.LocationData;
import com.pyforce.data.dto.WifiData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.entity.StringEntity;

/**
 * Created by Alex Coto on 17/2/16.
 *
 * TODO?: Acquire a wake lock from the power manager.
 */
public class LocatorService extends Service implements Runnable {

    public static String DEBUG_TAG = "LocatorService";

    private WifiBroadcastReceiver mBroadcastReceiver;
    private WifiManager mWifiManager;
    private Handler mHandler;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                Looper.prepare();
                Log.d(DEBUG_TAG, "Locator service started.");

                mBroadcastReceiver = new WifiBroadcastReceiver();

                mHandler = new Handler();
                mHandler.post(LocatorService.this);

                registerReceiver(mBroadcastReceiver,
                        new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION),
                        null, // No permissions required
                        mHandler // The handler for this thread
                );

                mWifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
                mWifiManager.startScan();

                Looper.loop();
            }
        }).start();



        return START_STICKY;
    }

    @Override
    public void run() {
        // If the data handler is null, it means
        // the server address hasn't been yet set.
        //
        // TODO: This should probably be refactored
        // so the service starts AFTER knowing the server address.

        if (DataHandler.get() != null) {

            List<WifiData> data = mBroadcastReceiver.getWifiData();

            if (data.size() > 0) {
                Log.d(DEBUG_TAG, "Starting POST...");
                DataHandler.get().sendWifiData(data, new JsonHttpResponseHandler() {
                    @Override
                    public void onPostProcessResponse(ResponseHandlerInterface instance, HttpResponse response) {
                        Log.d(DEBUG_TAG, "POST complete.");
                    }
                });
            } else {
                Log.d(DEBUG_TAG, "No wifi networks detected. Avoiding POST.");
            }
        }

        try {
            Thread.sleep(2000, Thread.NORM_PRIORITY);
        } catch (InterruptedException e) {
            Log.e(DEBUG_TAG, "Locator Service Thread interrupted.");
            Log.e(DEBUG_TAG, e.getMessage());
        }

        mHandler.postDelayed(this, 2000);
    }

    class WifiBroadcastReceiver extends BroadcastReceiver {

        CircularQueue<List<ScanResult>> queue = new CircularQueue<>(5);

        @Override
        public void onReceive(Context context, Intent intent) {
            queue.enqueue(mWifiManager.getScanResults());
            mWifiManager.startScan();
        }

        public List<WifiData> getWifiData() {
            return IntensityFilters.mean(queue);
        }
    }
}
