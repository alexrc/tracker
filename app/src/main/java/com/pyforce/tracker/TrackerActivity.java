package com.pyforce.tracker;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Matrix;
import android.net.Uri;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.provider.Settings.Secure;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.pyforce.data.DataHandler;
import com.pyforce.data.callbacks.DownloadedFileCallback;
import com.pyforce.data.dto.Fingerprint;
import com.pyforce.data.dto.Floor;
import com.pyforce.utils.ConnectionCallback;
import com.pyforce.utils.Point2D;
import com.pyforce.utils.Tuple;

import org.json.JSONArray;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

/**
 * This is the main Activity for the tracker app.
 *
 * Following sort of an MVC pattern, this would be the controller,
 * whereas the <code>DataHandler</code> delegates some control.
 *
 * The views would be... well, the Views.
 */
public class TrackerActivity extends AppCompatActivity implements
        View.OnTouchListener {

    /* Constants */
    private static final String DEBUG_TAG = "TrackerActivity";
    private static final int PERMISSIONS_REQUEST_CODE_ACCESS_COARSE_LOCATION = 0x64;

    /* Helper controller */
    private DataHandler dataController;

    /* Views */
    private OverlayMapView mapView;

    /* Wifi related */
    private WifiManager wm;

    /* Other */
    private GestureDetector gd;
    private Point2D currentPosition;

    private String deviceId;

    /**
     * Alert dialog for the server address
     */
    private AlertDialog.Builder mServerAddressBuilder;
    private View mAddressView;

    DisplayMetrics displayMetrics = new DisplayMetrics();

    /**
     * Initializes the Activity.
     *
     * TODO: Restore activity state from bundle.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tracker);

        // Initialize fields
        mapView = (OverlayMapView) findViewById(R.id.map_view);
        wm = (WifiManager) getSystemService(Context.WIFI_SERVICE);

        // TODO: Create a dialog to enable wifi if disabled.
        wm.setWifiEnabled(true);

        // Set up map touch events
        gd = new GestureDetector(this, new LongTouchOnMapHandler());

        // Receive touch events from the map
        mapView.setOnTouchListener(this);

        startService(new Intent(this, LocatorService.class));

        deviceId =  Secure.getString(TrackerActivity.this.getContentResolver(),
                Secure.ANDROID_ID);

        mAddressView = LayoutInflater.from(TrackerActivity.this)
                .inflate(R.layout.text_input_dialog, null);
        final EditText editText = (EditText) mAddressView.findViewById(R.id.floor_name_edit_text);

        final SharedPreferences preferences = getPreferences(Context.MODE_PRIVATE);
        String defaultValue = getResources().getString(R.string.default_ip_address);
        String defaultIp = preferences.getString(getString(R.string.default_ip_address), defaultValue);

        editText.setText(defaultIp);

        mServerAddressBuilder = new AlertDialog.Builder(TrackerActivity.this)
                .setView(mAddressView)
                .setPositiveButton(getString(R.string.OK), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String address = editText.getText().toString();

                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putString(getResources().getString(R.string.default_ip_address), address);
                        editor.apply();

                        // Initialize the controller
                        dataController = new DataHandler(TrackerActivity.this, deviceId);

                        final ProgressDialog pDialog = ProgressDialog.show(TrackerActivity.this,
                                getString(R.string.please_wait),
                                getString(R.string.connecting_to_server),
                                true, true,
                                new DialogInterface.OnCancelListener() {
                                    @Override
                                    public void onCancel(DialogInterface dialog) {
                                        ((ViewGroup) mAddressView.getParent()).removeView(mAddressView);
                                        mServerAddressBuilder.create().show();
                                        dataController.cancelPendingConnections();
                                    }
                                });

                        dataController.connect(address, new ConnectionCallback() {
                            @Override
                            public void onFailure() {
                                ((ViewGroup)mAddressView.getParent()).removeView(mAddressView);
                                mServerAddressBuilder.create().show();
                                pDialog.dismiss();
                            }

                            @Override
                            public void onSuccess() {
                                Toast.makeText(TrackerActivity.this, R.string.successful_connection,
                                        Toast.LENGTH_SHORT).show();
                                pDialog.dismiss();
                                changeFloor();
                            }
                        });


                    }
                })
                .setCancelable(false);

        mServerAddressBuilder.create().show();

        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
    }


    /**
     * Inflates the options menu.
     *
     * TODO: Remove if options are to be removed to a side panel.
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_activity_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * Called when a menu item option is selected.
     *
     * TODO: Change this to a side panel (more comfortable)
     * @param item the selected menu item
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_clear_current_fingerprints:
                dataController.clearCurrentFloorData();
                mapView.clearOverlays();
                break;

            case R.id.action_switch_floor:
                changeFloor();
                break;

            case R.id.action_undo_last_fingerprint:
                Fingerprint fingerprint = dataController.undoLastFingerprint();
                if (fingerprint != null) {
                    mapView.removeMarker(fingerprint.getPosition());
                } else {
                    Toast.makeText(TrackerActivity.this,
                            R.string.no_fingerprints_taken_in_current_session,
                            Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.action_connect_to_other_server:
                ((ViewGroup) mAddressView.getParent()).removeView(mAddressView);
                dataController.cancelPendingConnections();
                mServerAddressBuilder.create().show();
                break;

            case R.id.action_show_locations:
                Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                        dataController.getUrlForDeviceMap());
                startActivity(browserIntent);
                break;

            case R.id.action_show_about_dialog:
                // TODO: Change toast for a full view.
                Toast.makeText(TrackerActivity.this,
                        "Tracker - v" + getVersion(),
                        Toast.LENGTH_SHORT).show();
                break;

            default:
                Log.e(DEBUG_TAG, "Clicked on unknown option");
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void changeFloor() {
        dataController.loadFloorListForCurrentPlaceAsync(new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                PickDialogFragment fragment = new PickDialogFragment();

                fragment.setAdapter(dataController.getFloorAdapter());
                fragment.show(getSupportFragmentManager(), "floor_picker");
                fragment.setOptionPickedListener(new PickDialogFragment.OnDialogItemPickedListener() {
                    @Override
                    public void onOptionPicked(final Object element) {
                        dataController.setCurrentFloor(((Tuple<Integer, String>) element).A);
                        dataController.loadCurrentFloorImageUri(new DownloadedFileCallback() {
                            @Override
                            public void onFileDownloaded(File fileUri) {
                                mapView.setImageURI(Uri.fromFile(fileUri));
                                Toast.makeText(TrackerActivity.this, "Loaded " +
                                                ((Tuple<Integer, String>) element).B + " map.",
                                        Toast.LENGTH_SHORT).show();
                            }
                        });
                        dataController.setFloorChangeListener(new DataHandler.FloorChangeListener() {
                            @Override
                            public void onFloorChange(Floor newFloor) {
                                mapView.clearOverlays();
                                for (Fingerprint f :
                                        newFloor.Fingerprints) {
                                    mapView.addOverlay(f.getScaledPosition(1 / displayMetrics.density));
                                }
                            }
                        });
                    }

                    @Override
                    public void onCancelOptionPicked() {
                        Log.d(DEBUG_TAG, "Canceled floor picking");
                    }
                });

            }
        });
    }

    /**
     * Delegates touch events to the gesture detector.
     */
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        // Use the gesture detector
        gd.onTouchEvent(event);

        return true;
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);

        // TODO: Save map info
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        // TODO: Load map info
    }

    public String getVersion() {
        return "1.0";
    }


    /**
     * Callback for fingerprinting.
     */
    class FingerprintHandler extends BroadcastReceiver{

        List<List<ScanResult>> mTotalResults = new ArrayList<>();
        ProgressDialog pd;
        long lastSample;

        public FingerprintHandler(){
            startProgressDialog();
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            List<ScanResult> results = wm.getScanResults();

            if (results.size() > 0) {

                long currentTime = System.currentTimeMillis();

                // If at least some time has passed since the last sample...
                if (currentTime - lastSample >= 2000){

                    updateFingerprintGatheringProgress(results);

                    if (mTotalResults.size() >= 5) {
                        finishFingerprintGathering();
                    }

                    lastSample = currentTime;

                } else {
                    // Too frequent: just ignore this sample.
                }
            }
            else {
                Toast.makeText(TrackerActivity.this, getString(R.string.no_networks_detected),
                        Toast.LENGTH_SHORT).show();

                pd.dismiss();
                unregisterReceiver(this);
                mapView.clearTemporaryOverlay();
            }
        }

        private void updateFingerprintGatheringProgress(List<ScanResult> results) {
            mTotalResults.add(results);
            pd.incrementProgressBy(20); // TODO: Configure the amount of samples
        }

        private void finishFingerprintGathering() {
            mapView.addOverlay(currentPosition);
            dataController.placeFingerprint(currentPosition.copy()
                    .scale(displayMetrics.density),
                    mTotalResults);

            pd.dismiss();

            Toast.makeText(TrackerActivity.this,
                R.string.fingerprint_taken,
                Toast.LENGTH_SHORT).show();
            unregisterReceiver(this);
        }

        private void startProgressDialog() {
            pd = new ProgressDialog(TrackerActivity.this);
            pd.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            pd.setTitle(getString(R.string.taking_fingerprint));
            pd.setMessage(getString(R.string.fingerprinting_advice));
            pd.setCancelable(true);
            pd.setIndeterminate(false);
            pd.show();

            pd.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    pd.dismiss();
                    unregisterReceiver(FingerprintHandler.this);
                    mapView.clearTemporaryOverlay();

                    Toast.makeText(TrackerActivity.this,
                            R.string.fingerprint_cancelled,
                            Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    /**
     * Long touch handler.
     *
     * Currently places a gray overlay on the map and sets off the wifi scanning.
     */
    class LongTouchOnMapHandler extends GestureDetector.SimpleOnGestureListener {

        @Override
        public void onLongPress(MotionEvent e) {
            super.onLongPress(e);

            Matrix imageMatrix = mapView.getImageMatrix();
            Matrix inverse = new Matrix(imageMatrix);

            if (imageMatrix.invert(inverse)) {
                float[] points = new float[]{e.getX(), e.getY()};
                inverse.mapPoints(points);

                currentPosition = new Point2D(points[0], points[1]);

                registerReceiver(
                        new FingerprintHandler(),
                        new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));

                mapView.addTemporalPoint(currentPosition);

                doGetWifi();

                Log.d(DEBUG_TAG, String.format(getString(R.string.started_scan_at), points[0], points[1]));
            } else {
                Log.e(DEBUG_TAG, "This image matrix is not invertible!!!");
            }
        }
    }

    private void doGetWifi() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                    PERMISSIONS_REQUEST_CODE_ACCESS_COARSE_LOCATION);
            // After this point you wait for callback in
            // onRequestPermissionsResult(int, String[], int[]) overriden method
        } else {
            wm.startScan();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_CODE_ACCESS_COARSE_LOCATION
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            // Do something with granted permission
            doGetWifi();
        }
    }
}
