package com.pyforce.tracker;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.widget.ListAdapter;

/**
 * Dialog Fragment that allows the user to switch floors/places.
 */
public class PickDialogFragment extends android.support.v4.app.DialogFragment{

    final String DEBUG_TAG = "PickDialog";

    /**
     * Reference to the data handler.
     * Should be set by calling <code>setAdapter</code> before showing the fragment
     */
    ListAdapter dataAdapter;

    public void setAdapter(ListAdapter data){
        dataAdapter = data;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@NonNull Bundle savedInstanceState) {
        if (dataAdapter == null)
            throw new UnsupportedOperationException("You need to call setAdapter first.");

        // Use the Builder class for convenient dialog construction
        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(R.string.action_select_floor);
        builder.setAdapter(
                dataAdapter,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        callback.onOptionPicked(dataAdapter.getItem(which));
                    }
                });

        // Create the AlertDialog object and return it
        return builder.create();
    }


    /**
     * Allows for notification of a floor/place being picked.
     */
    public interface OnDialogItemPickedListener {
        void onOptionPicked(Object selected);
        void onCancelOptionPicked();
    }

    private OnDialogItemPickedListener callback;

    public void setOptionPickedListener(OnDialogItemPickedListener floorPickedListener) {
        callback = floorPickedListener;
    }

}

