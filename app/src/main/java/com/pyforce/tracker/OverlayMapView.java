package com.pyforce.tracker;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;

import com.ortiz.touch.TouchImageView;
import com.pyforce.utils.Point2D;

import java.util.ArrayList;
import java.util.List;

/**
 * Convenience class for map and overlays.
 *
 * Extends TouchImageView from Mike Ortiz
 * (code from public domain)
 */
public class OverlayMapView extends TouchImageView {

    // TODO: Change constants so size is consistent among
    // different screen sizes and pixel densities

    final int OVERLAY_WIDTH = 50;
    final int OVERLAY_HEIGHT = 82;

    final int OVERLAY_SHADOW_WIDTH = 41;
    final int OVERLAY_SHADOW_HEIGHT = 41;

    private final Drawable overlayIcon;
    private final Drawable grayOverlayIcon;
    private final Drawable overlayShadow;

    // TODO: Order by Y coords to avoid incorrect drawing order
    protected List<Point2D> overlayPositions = new ArrayList<>();
    protected Point2D currentScanPoint = null;

    public OverlayMapView(Context context, AttributeSet attrs) {
        super(context, attrs);

        overlayIcon = getResources().getDrawable(R.drawable.marker_icon_2x);
        grayOverlayIcon = getResources().getDrawable(R.drawable.marker_icon_2x_gray);
        overlayShadow = getResources().getDrawable(R.drawable.marker_shadow);

        setMaxZoom(10);
    }

    public void addOverlay(Point2D point) {
        if (currentScanPoint != null && currentScanPoint.equals(point)){
            currentScanPoint = null;
        }

        overlayPositions.add(point.copy());
        invalidate();
    }

    public void addTemporalPoint(Point2D point){
        currentScanPoint = point;
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        for (Point2D f : overlayPositions) {
            drawOverlayPoint(canvas, (float) f.X, (float) f.Y, false);
        }

        if (currentScanPoint != null){
            drawOverlayPoint(canvas, (float) currentScanPoint.X, (float) currentScanPoint.Y, true);
        }
    }

    private void drawOverlayPoint(Canvas c, float x, float y, boolean gray) {
        float[] xy = new float[]{x, y};
        getImageMatrix().mapPoints(xy);

        int overlayX = (int) xy[0] - OVERLAY_WIDTH / 2;
        int overlayY = (int) xy[1] - OVERLAY_HEIGHT;

        if(!gray){
            overlayIcon.setBounds(
                    overlayX,
                    overlayY,
                    overlayX + OVERLAY_WIDTH,
                    overlayY + OVERLAY_HEIGHT);

            overlayIcon.draw(c);
        } else {
            grayOverlayIcon.setBounds(
                    overlayX,
                    overlayY,
                    overlayX + OVERLAY_WIDTH,
                    overlayY + OVERLAY_HEIGHT);

            grayOverlayIcon.draw(c);
        }


        // TODO: correctly position the shadow
//            int shadowX = (int) xy[0] + OVERLAY_WIDTH / 2;
//            int shadowY = (int) xy[1] + OVERLAY_HEIGHT;
//
//            overlayShadow.setBounds(
//                    shadowX,
//                    shadowY,
//                    shadowX + OVERLAY_SHADOW_WIDTH,
//                    shadowY + OVERLAY_SHADOW_HEIGHT
//            );
//            overlayShadow.draw(canvas);


    }

    public void clearOverlays(){
        overlayPositions.clear();
        invalidate();
    }

    public void clearTemporaryOverlay(){
        currentScanPoint = null;
        invalidate();
    }

    public void removeMarker(Point2D position) {
        overlayPositions.remove(position);
        invalidate();
    }
}
